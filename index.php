<?php
// Aufbau zur Datenbank
$db = new mysqli('localhost', 'root', 'root', 'adressverwaltung');

// Fehlerausgabe
error_reporting(0);
$db = new mysqli('localhost', 'root', 'root', 'adressverwaltung');
if ($db->connect_errno) {
    die('Sorry - gerade gibt es ein Problem');
}
//Darstellungsfehler üäö vorbeugen
$db = set_charset ('utf8');

//Anfrage Datensätze
$erg = $db->query("SELECT id, vorname, name, adress FROM adressverwaltung");

// Anzahl getroffene Datensätze
print_r($erg);
if ($erg->num_rows) {
	echo "<p>Daten vorhanden: Anzahl ";
	echo $erg->num_rows;
}
// Ausgabe Datensatz anhand Suchbegriff
$suche_nach = "%{$suchbegriff}%";
$suche = $db->prepare("SELECT id, vorname, name, adress
                       FROM adressverwaltung
                       WHERE name LIKE ? OR vorname LIKE ?");
$suche->bind_param('ss', $suche_nach, $suche_nach);
$suche->execute();
$suche->bind_result($id, $vorname, $name, $adress);
while ($suche->fetch()) {
    echo "<li>";
	echo $id .' '. $vorname .' '. $name .' '. $adress;
	
// Aufräumen
$erg->free();
$db->close();

