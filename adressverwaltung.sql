-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 14. Dez 2018 um 20:19
-- Server-Version: 10.1.37-MariaDB
-- PHP-Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `adressverwaltung`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `adressverwaltung`
--

CREATE TABLE `adressverwaltung` (
  `id` int(11) NOT NULL,
  `vorname` varchar(11) COLLATE latin1_german2_ci NOT NULL,
  `name` varchar(11) COLLATE latin1_german2_ci NOT NULL,
  `adress` tinytext COLLATE latin1_german2_ci NOT NULL,
  `plz` int(4) NOT NULL,
  `ort` varchar(11) COLLATE latin1_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

--
-- Daten für Tabelle `adressverwaltung`
--

INSERT INTO `adressverwaltung` (`id`, `vorname`, `name`, `adress`, `plz`, `ort`) VALUES
(1, 'Kevin', 'Dutler', 'Wildhausersstrasse 17', 9473, 'Gams'),
(2, 'Peter', 'Pimmel', 'Arschstrasse 1', 9470, 'Buchs');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `adressverwaltung`
--
ALTER TABLE `adressverwaltung`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `adressverwaltung`
--
ALTER TABLE `adressverwaltung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
